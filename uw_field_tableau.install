<?php

/**
 * @file
 */

/**
 * Implements hook_field_schema().
 */
function uw_field_tableau_field_schema($field) {
  $columns = array(
    'server' => array(
      'type' => 'varchar',
      'length' => 255,
    ),
    'site' => array(
      'type' => 'varchar',
      'length' => 255,
    ),
    'url' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
    ),
    'height' => array(
      'type' => 'int',
      'length' => 5,
      'size' => 'small',
      'not null' => TRUE,
    ),
    'tabs' => array(
      'type' => 'int',
      'length' => 1,
      'size' => 'tiny',
      'not null' => TRUE,
    ),
  );

  $indexes = array(
    'url' => array('url'),
  );

  return array(
    'columns' => $columns,
    'indexes' => $indexes,
  );
}

/**
 * Add server column to each tableau field.
 */
function uw_field_tableau_update_7101() {
  $spec = [
    'type' => 'varchar',
    'length' => 255,
  ];
  $fields = _uw_field_tableau_get_mymodule_fields();

  foreach ($fields as $field) {
    $table_prefixes = array(
      _field_sql_storage_tablename($field),
      _field_sql_storage_revision_tablename($field)
    );
    foreach ($table_prefixes as $table_prefix) {

      $field_name = $field['field_name']; // eg 'field_dimensions' ;
      $table = $table_prefix . '_' . $field_name;
      //$table = $field_name;

      db_add_field($table_prefix, $field['field_name'] . '_' . 'server', $spec);
    }
  }
  return t('Added server column to all tableau fields in database.');
  
}

/**
 * Add site column to each tableau field.
 */
function uw_field_tableau_update_7102() {
  $spec = [
    'type' => 'varchar',
    'length' => 255,
  ];
  $fields = _uw_field_tableau_get_mymodule_fields();

  foreach ($fields as $field) {
    $table_prefixes = array(
      _field_sql_storage_tablename($field),
      _field_sql_storage_revision_tablename($field)
    );
    foreach ($table_prefixes as $table_prefix) {

      $field_name = $field['field_name']; // eg 'field_dimensions' ;
      $table = $table_prefix . '_' . $field_name;
      //$table = $field_name;

      db_add_field($table_prefix, $field['field_name'] . '_' . 'site', $spec);
    }
  }
  return t('Added site column to all tableau fields in database.');
}

/**
  * Returns all fields created on the system of the type defined in mymodule.
  */ 
function _uw_field_tableau_get_mymodule_fields() {
  $types = array_keys(uw_field_tableau_field_info()); // field types defined in mymodule
  $fields = array();
  foreach (field_info_fields() as $field) {
    if (in_array($field['type'], $types)) {
      $fields[] = $field;
    }
  }
  return $fields;
}
