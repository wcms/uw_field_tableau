<?php

/**
 * @file
 */

 /**
  * Implements hook_field_info().
  */
function uw_field_tableau_field_info() {
  return array(
    'uw_field_tableau_embed' => array(
      'label' => t('UWaterloo Tableau'),
      'description' => t('This field stores the necessary info about a UWaterloo Tableau embed in the database.'),
      'default_widget' => 'uw_field_tableau_textfield',
      'default_formatter' => 'uw_field_tableau_field',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function uw_field_tableau_field_widget_info() {
  return array(
    'uw_field_tableau_textfield' => array(
      'label' => t('UWaterloo Tableau fields'),
      'field types' => array('uw_field_tableau_embed'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function uw_field_tableau_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  if ($field['cardinality'] == 1) {
    $element['#type'] = 'fieldset';
  }

  $element['server'] = array(
    '#type' => 'select',
    '#title' => t('Server:'),
    '#options' => array(
      'legacy' => t('Legacy'),
      'private' => t('New - Private'),
      'public' => t('New - Public'),
    ),
    '#default_value' => isset($items[$delta]['server']) ? $items[$delta]['server'] : '',
  );
  $element['site'] = array(
    '#type' => 'textfield',
    '#title' => t('Site name:'),
    '#default_value' => isset($items[$delta]['site']) ? $items[$delta]['site'] : '',
  );
  $element['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Tableau name:'),
    '#default_value' => isset($items[$delta]['url']) ? $items[$delta]['url'] : '',
    '#required' => $element['#required'],
  );
  $element['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Tableau height in pixels:'),
    '#field_suffix' => '<br>' . t('(minimum 100)'),
    '#size' => 5,
    '#default_value' => isset($items[$delta]['height']) ? $items[$delta]['height'] : '',
    '#required' => $element['#required'],
  );
  $element['tabs'] = array(
    '#type' => 'select',
    '#title' => t('Display tabs:'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => isset($items[$delta]['tabs']) ? $items[$delta]['tabs'] : '',
  );

  // Required state is set on individual form elements.
  unset($element['#required']);

  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function uw_field_tableau_field_is_empty($item, $field) {
  if (empty($item['url']) || empty($item['height'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_validate().
 */
function uw_field_tableau_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['height'])) {
      // Make sure it's a valid whole number >=100.
      if (!is_numeric($item['height']) || $item['height'] < 100 || $item['height'] != floor($item['height'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'uw_field_tableau_length',
          'message' => t('%name: you must enter a valid whole number for the height.', array('%name' => $instance['label'])),
        );
      }

      // If there's a height, they need the name.
      if ($item['url'] == '') {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'uw_field_tableau_url',
          'message' => t('%name: you must enter the name of the Tableau visualization.', array('%name' => $instance['label'])),
        );
      }
    }
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function uw_field_tableau_field_formatter_info() {
  return array(
    'uw_field_tableau_field' => array(
      'label' => t('Tableau embed'),
      'field types' => array('uw_field_tableau_embed'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function uw_field_tableau_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, &$items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'uw_field_tableau_field':
      $i = -1;
      foreach ($items as $key => $value) {
        $i++;
        $id = drupal_html_id("embedded_tableau-$i");
        $tabs = $value['tabs'] == 1 ? 'yes' : 'no';
        $tableau_siteroot = '';
        if (isset($value['site'])) {
          $tableau_site = '/t/' . check_plain($value['site']);
        }
        else {
          $tableau_site = '';
        }
        // Get server to use.
        if (isset($value['server']) && $value['server'] == 'private') {
          $host_url = "https://iap-tableau.private.uwaterloo.ca/";
          $tableau_siteroot = '/t/IAP_Private';
        }
        elseif (isset($value['server']) && $value['server'] == 'public') {
          $host_url = "https://public.tableau.com/";
        }
        else {
          // If set to "legacy" or not set.
          $host_url = "https://iap-reporting.uwaterloo.ca/";
        }
        $tableau_object = 
          "<div class='tableauPlaceholder' id='". $id . "' style='position: relative'>
            <noscript>
              <a href='#'><img alt='image placeholder for tableau' src='" . drupal_get_path('module', 'ckeditor_socialmedia') . '/images/tableau_loading.jpg' . " style='border: none' /></a>
            </noscript>
            <object class='tableauViz'  style='display:none;'>
              <param name='host_url' value='" . $host_url . "' />
	      <param name='embed_code_version' value='3' />
	      <param name='site_root' value='" . $tableau_site . "' />
	      <param name='name' value='" . check_url(decode_entities(decode_entities($value['url']))) . "' />
	      <param name='tabs' value='" . $tabs . "' />
	      <param name='toolbar' value='no' />
	      <param name='static_image' value='" . drupal_get_path('module', 'ckeditor_socialmedia') . '/images/tableau_loading.jpg' . "' />
	      <param name='animate_transition' value='yes' />
	      <param name='display_static_image' value='yes' />
	      <param name='display_spinner' value='yes' />
	      <param name='display_overlay' value='yes' />
	      <param name='display_count' value='yes' />
            </object>
          </div>
          <script type='text/javascript'>
            var divElement = document.getElementById('" . $id . "');
            var vizElement = divElement.getElementsByTagName('object')[0];
            vizElement.style.width='100%';vizElement.style.height='" . check_plain($value['height']) . "px';
            var scriptElement = document.createElement('script');
            scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';
            vizElement.parentNode.insertBefore(scriptElement, vizElement);
          </script>";

        $markup = '<div class="tableauWrapper">';
        $markup .= $tableau_object;
        $markup .= '</div>';
        $element[$key] = array(
          '#type' => 'markup',
          '#markup' => $markup,
        );
      }
      break;
  }
  return $element;
}
